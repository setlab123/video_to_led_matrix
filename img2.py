import cv2
import numpy as np
from tempfile import TemporaryFile
import copy

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import struct
import serial
import time


def frame_to_led(newimg):
    kwol = 4

    blue = newimg[:,:,0]/255 * kwol
    green = newimg[:,:,1]/255 * kwol
    red = newimg[:,:,2]/255 * kwol

    blue = np.around(blue)
    green = np.around(green)
    red = np.around(red)

    blue.astype(int)
    green.astype(int)
    red.astype(int)







    red_2 = copy.deepcopy(red)
    red_1 = copy.deepcopy(red)


    red_bin = []
    for z in range(1,kwol+1):
        red_bin.append([])
        for j in range(32):
            for i in range(64):
                if (red_1[j][i] != 0):
                    red_1[j][i] = red_1[j][i] - 1

        red_3 = red_2 - red_1
        red_2 = copy.deepcopy(red_1)
        for j in range(32):
            str_red = ""
            for i in range(64):
                str_red = str_red +str((int(red_3[j][i])))
            red_bin[z-1].append((int(str_red,2)))

    # print(red_bin)


    green_2 = copy.deepcopy(green)
    green_1 = copy.deepcopy(green)


    green_bin = []
    for z in range(1,kwol+1):
        green_bin.append([])
        for j in range(32):
            for i in range(64):
                if (green_1[j][i] != 0):
                    green_1[j][i] = green_1[j][i] - 1

        green_3 = green_2 - green_1
        green_2 = copy.deepcopy(green_1)
        for j in range(32):
            str_green = ""
            for i in range(64):
                str_green = str_green +str((int(green_3[j][i])))
            green_bin[z-1].append((int(str_green,2)))

    # print(green_bin)



    blue_2 = copy.deepcopy(blue)
    blue_1 = copy.deepcopy(blue)


    blue_bin = []
    for z in range(1,kwol+1):
        blue_bin.append([])
        for j in range(32):
            for i in range(64):
                if (blue_1[j][i] != 0):
                    blue_1[j][i] = blue_1[j][i] - 1

        blue_3 = blue_2 - blue_1
        blue_2 = copy.deepcopy(blue_1)
        for j in range(32):
            str_blue = ""
            for i in range(64):
                str_blue = str_blue +str((int(blue_3[j][i])))
            blue_bin[z-1].append((int(str_blue,2)))

    # print(blue_bin)

    # serial.write((red_bin))

    for i in range(kwol):
        for j in range(32):
            serial.write(struct.pack('<Q',red_bin[i][j]))

    for i in range(kwol):
        for j in range(32):
            serial.write(struct.pack('<Q',green_bin[i][j]))

    for i in range(kwol):
        for j in range(32):
            serial.write(struct.pack('<Q',blue_bin[i][j]))





serial = serial.Serial(port='COM5', baudrate=57600,)

cap = cv2.VideoCapture('video.mp4')


start = time.time()
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  W = 64.
  height, width, depth = frame.shape
  print(frame.shape)
  imgScale = W/width
  newX,newY = frame.shape[1]*imgScale, frame.shape[0]*imgScale
  newimg = cv2.resize(frame,(int(newX),int(newY)))
  print(newimg.shape)
  if ret == True:
    # Display the resulting frame
    cv2.imshow('Frame',frame)

    delta_time=time.time()-start
    if delta_time>0.8:
        frame_to_led(newimg)
        start = time.time()

    # Press Q on keyboard to  exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
      break

  # Break the loop
  else:
    break

cap.release()
cv2.destroyAllWindows()



# filename = 'krosh.jpg'
# W = 64.
# oriimg = cv2.imread(filename)
# height, width, depth = oriimg.shape
# imgScale = W/width
# newX,newY = oriimg.shape[1]*imgScale, oriimg.shape[0]*imgScale
# newimg = cv2.resize(oriimg,(int(newX),int(newY)))
