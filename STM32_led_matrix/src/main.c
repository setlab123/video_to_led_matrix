
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart3_rx;
DMA_HandleTypeDef hdma_usart3_tx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void clock(void);
void lat(void);
void oe(void);


typedef struct{
	uint64_t red_bin_u[4][32];
	uint64_t green_bin_u[4][32];
	uint64_t blue_bin_u[4][32];

} Serialcommand;


volatile Serialcommand command;



uint64_t red_bin[4][32]={{0xfe0000000000007f, 0xfe0000001f80007f, 0xfe000001fff8007f, 0xfe000007fffe007f, 0xfe00001fffff007f, 0xfe00007fffff807f, 0xfe0001ffffffc07f, 0xfe0003ffffffe07f, 0xfe0007fffffff07f, 0xfe000ffffffff87f, 0xfe001ffffffff87f, 0xfe003ffffffffc7f, 0xfe007ffffffffc7f, 0xfe00fffffffffe7f, 0xfe00fffffffffe7f, 0xfe01fffffffffe7f, 0xfe01ffffffffff7f, 0xfe03ffffffffff7f, 0xfe03ffffffffff7f, 0xfe07ffffc00fff7f, 0xfe1ffff8fffcff7f, 0xfe3fffcfffff3e7f, 0xfe3ffe7fffffee7f, 0xfe7ffdfff801f07f, 0xfefff7ffcfff387f, 0xfeffefff7ff0de7f, 0xfeffdff87fe0677f, 0xffff3fc01feff37f, 0xfeff7ff7effff07f, 0xfefbfffff7fff07f, 0xfe4ffffffffff87f, 0xfe3ffffffffff87f},
{0xfe0000000000007f, 0xfe0000000f00007f, 0xfe000000fff0007f, 0xfe000007fffc007f, 0xfe00001fffff007f, 0xfe00007fffff807f, 0xfe0000ffffffc07f, 0xfe0003ffffffe07f, 0xfe0007fffffff07f, 0xfe000ffffffff87f, 0xfe001ffffffff87f, 0xfe003ffffffffc7f, 0xfe007ffffffffc7f, 0xfe00fffffffffe7f, 0xfe00fffffffffe7f, 0xfe01fffffffffe7f, 0xfe01fffffffffe7f, 0xfe03fffffffffe7f, 0xfe03ffffffffff7f, 0xfe03ffff8007fe7f, 0xfe0ffff0fff8fe7f, 0xfe1fff8fffff1e7f, 0xfe3ffe7fffffcc7f, 0xfe7ff9fff801f07f, 0xfefff3ffc7fe387f, 0xfeffeffe3ff0ce7f, 0xfeff9ff83fe0677f, 0xfefe1f801fefe37f, 0xfefe30e7cfdff07f, 0xfef1fffff7fff07f, 0xfe4ffffffffff87f, 0xfe3ffffffffff87f},
{0xfc0000000000003f, 0xfc0000000000003f, 0xfc000000fff0003f, 0xfc000007fffc003f, 0xfc00001fffff003f, 0xfc00007fffff803f, 0xfc0000ffffffc03f, 0xfc0003ffffffe03f, 0xfc0007fffffff03f, 0xfc000ffffffff03f, 0xfc001ffffffff83f, 0xfc003ffffffffc3f, 0xfc007ffffffffc3f, 0xfc007ffffffffc3f, 0xfc00fffffffffe3f, 0xfc01fffffffffe3f, 0xfc01fffff83ffe3f, 0xfc03ffffe03ffe3f, 0xfc03fffffffffe3f, 0xfc03ffff0003fe3f, 0xfc07fff07ff07e3f, 0xfc17ff87ffff1e3f, 0xfc3ffe3fffffcc3f, 0xfc7ff9fff000f03f, 0xfc7ff3ff87fc383f, 0xfcffcffe3ff08c3f, 0xfcfd9ff03fe0663f, 0xfcfe0f001fef613f, 0xfcfe0067cfdfb03f, 0xfce09ffff7fff03f, 0xfc048ffffffff03f, 0xfc3e0ffffffff83f},
{0xfc0000000000003f, 0xfc0000000000003f, 0xfc000000fff0003f, 0xfc000003fffc003f, 0xfc00001fffff003f, 0xfc00003fffff803f, 0xfc0000ffffffc03f, 0xfc0001ffffffe03f, 0xfc0007fffffff03f, 0xfc000ffdfb7df03f, 0xfc001fe1f03e183f, 0xfc003f03f03e003f, 0xfc007c03c006003f, 0xfc0070010000003f, 0xfc00c0000000003f, 0xfc0080000000003f, 0xfc0100000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000003ff0003f, 0xfc000007fffe003f, 0xfc00003fc000003f, 0xfc0000fe0000003f, 0xfc0003f803f8003f, 0xfc000fe03ff0803f, 0xfc001fc03fe0603f, 0xfc0000000fef203f, 0xfc000023cfdfb03f, 0xfc000fcff7fff03f, 0xfc000ffffffff03f, 0xfc3e0ffffff7f03f},
};



uint64_t green_bin[4][32]={{0xfe0000000000007f, 0xfe0000000000007f, 0xfe000000fff0007f, 0xfe000007fffc007f, 0xfe00001fffff007f, 0xfe00007fffff807f, 0xfe0000ffffffc07f, 0xfe0003ffffffe07f, 0xfe0007fffffff07f, 0xfe000ffffffff07f, 0xfe001fe1fb7e387f, 0xfe003f03f03e007f, 0xfe007c03f4be007f, 0xfe007003e49e007f, 0xfe00e001efde007f, 0xfe018001efdc007f, 0xfe010000fff8007f, 0xfe0000003ff0007f, 0xfe0000000700007f, 0xfe0000000000007f, 0xfe0000007ff8007f, 0xfe10000fffff007f, 0xfe20003ff000007f, 0xfe4001ff0000007f, 0xfe0003f80fff007f, 0xfe000ff07ff0c07f, 0xfe001fe03fe0607f, 0xfe001f001feff07f, 0xfe007ff7effff07f, 0xfe00fffff7fff07f, 0xfe0ffffffffff87f, 0xfe3ffffffffff87f},
{0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000200007f, 0xfe0000003fe0007f, 0xfe000000fff8007f, 0xfe000001fb7c007f, 0xfe000001f03e007f, 0xfe000003f03e007f, 0xfe000003e01e007f, 0xfe000003e49e007f, 0xfe000001cfcc007f, 0xfe000001cfcc007f, 0xfe0000007ff8007f, 0xfe0000003fe0007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe00000007fc007f, 0xfe0000003ff0c07f, 0xfe0000003fe0607f, 0xfe0000001fef607f, 0xfe002067cfdff07f, 0xfe009ffff7fff07f, 0xfe0e0ffffffff07f, 0xfe3e0ffffffff87f},
{0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000003fe0003f, 0xfc0000007b78003f, 0xfc000000fb7c003f, 0xfc000001f03c003f, 0xfc000001f03e003f, 0xfc000003e01e003f, 0xfc000001e49e003f, 0xfc000001c68c003f, 0xfc000000cfcc003f, 0xfc0000007c70003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc00000003f8003f, 0xfc0000003ff0803f, 0xfc0000003fe0603f, 0xfc0000000fef203f, 0xfc000027cfdfb03f, 0xfc001feff7fff03f, 0xfc000ffffffff03f, 0xfc3e0ffffffff03f},
{0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000001fe0003f, 0xfc0000007b78003f, 0xfc000000fb7c003f, 0xfc000001f03c003f, 0xfc000001f03e003f, 0xfc000001c006003f, 0xfc0000010000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc000000000e003f, 0xfc000003e01f003f, 0xfc000007f03f803f},
};



uint64_t blue_bin[4][32]={{0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000700007f, 0xfe0000003ff0007f, 0xfe000000fff8007f, 0xfe000001fb7c007f, 0xfe000001f37e007f, 0xfe000003f03e007f, 0xfe000003e4be007f, 0xfe000003e49e007f, 0xfe000001efce007f, 0xfe000001cfcc007f, 0xfe000000fff8007f, 0xfe0000003fe0007f, 0xfe0000000200007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000ffe007f, 0xfe0000003ff0c07f, 0xfe0000003fe0607f, 0xfe0000001feff07f, 0xfe0039f7efdff07f, 0xfe00dffff7fff07f, 0xfe0fcffffffff87f, 0xfe3fcffffffff87f},
{0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000003fe0007f, 0xfe000000fff8007f, 0xfe000001fb7c007f, 0xfe000001f03e007f, 0xfe000003f03e007f, 0xfe000003e01e007f, 0xfe000003e49e007f, 0xfe000001cfcc007f, 0xfe000001cfcc007f, 0xfe0000007ff8007f, 0xfe0000003fe0007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000000000007f, 0xfe0000003f00007f, 0xfe0000003fe0007f, 0xfe0000000fc0007f, 0xfe000023cfde007f, 0xfe000feff7ff007f, 0xfe000fffffffc07f, 0xfe3e0fffffffe07f},
{0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000003fe0003f, 0xfc0000007b78003f, 0xfc000000fb7c003f, 0xfc000001f03c003f, 0xfc000001f03e003f, 0xfc000003e01e003f, 0xfc000001e49e003f, 0xfc000001c68c003f, 0xfc000000cfcc003f, 0xfc0000007830003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc000000000e003f, 0xfc000003e01f803f, 0xfc000007f03f803f},
{0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000001fe0003f, 0xfc0000007b78003f, 0xfc000000fb7c003f, 0xfc000001f03c003f, 0xfc000001f03e003f, 0xfc000001c006003f, 0xfc0000010000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc0000000000003f, 0xfc000000000e003f, 0xfc000003e01f003f, 0xfc000007f01f803f},
};











int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART3_UART_Init();
  MX_TIM1_Init();
  HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  int capture = 0;
  char buf[25];

//  int Y = 10;
  int X = 0;
  int PANEL_X_MAX=128;


  memcpy(command.red_bin_u, red_bin, sizeof(command.red_bin_u));
  memcpy(command.green_bin_u, green_bin, sizeof(command.green_bin_u));
  memcpy(command.blue_bin_u, blue_bin, sizeof(command.blue_bin_u));

  while (1)
  {
	  capture = TIM1->CNT;
	  X = round(capture/100);
//	  sprintf(buf,"count: [%ld]\n", sizeof(red_bin));
//	  HAL_UART_Transmit_DMA(&huart3,buf,sizeof(buf)-1);

	  HAL_UART_Receive_DMA(&huart3, (int *)&command, sizeof(red_bin)*3-1);


	  memcpy(red_bin, command.red_bin_u, sizeof(red_bin));
	  memcpy(green_bin, command.green_bin_u, sizeof(green_bin));
	  memcpy(blue_bin, command.blue_bin_u, sizeof(blue_bin));



	  int Y = 0;

	  for(int z = 0; z<4; z++){
	  for( int j = 0; j<16;j++){
//		  HAL_Delay(1);
//		  for(int d=0;d<0x500;d++);
//		  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);

		  Y= j;

		  set_row(Y);
//		  Y++;


//		  } // for X


		  uint64_t one = 1;
		  for (int i = 0; i < 64; i++)
		  {
			  //top
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin[z][Y]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin[z][Y]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin[z][Y]>>(63-i)&one));
			  //bottom
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin[z][Y+16]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin[z][Y+16]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin[z][Y+16]>>(63-i)&one));
			  clock();
		  } // for X
//		  for (int i = 0; i < 64; i++)
//		  {
//			  //top
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin[z][Y+32]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin[z][Y+32]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin[z][Y+32]>>(63-i)&one));
//			  //bottom
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin[z][Y+48]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin[z][Y+48]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin[z][Y+48]>>(63-i)&one));
//			  clock();
//		  } // for X
		  lat();
		  oe();
//		  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_RESET);
	  } // for Y
	  }//z


	} //while

  } //main

void clock(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,GPIO_PIN_SET);
}

void lat(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_SET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);
}
void oe(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_RESET);
	  for(int d=0;d<0x50;d++);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
}

void set_row(int row){
	  if (row & 0x01) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_RESET);

	  if (row & 0x02) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_RESET);

	  if (row & 0x04) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_RESET);

	  if (row & 0x08) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_RESET);
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{

  TIM_Encoder_InitTypeDef sConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 12800;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 57600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA2 PA3
                           PA4 PA5 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB3 PB4 PB5 PB6
                           PB7 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
